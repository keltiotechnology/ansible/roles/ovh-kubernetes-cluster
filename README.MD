# OVH Kubernetes cluster

Create a kubernetes cluster on OVH.

## Local prerequisites
- pip packages: ovh + requests (see requirements.txt)

## Vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| ovh_kubernetes_cluster_name | The cluster name to create or delete | `string` | `` | yes |
| secret_ovh_application_key | The ovh application key | `string` | `` | yes |
| secret_ovh_application_secret | The ovh application secret | `string` | `` | yes |
| secret_ovh_consumer_key | The ovh consumer key | `string` | `` | yes |
| ovh_kubernetes_serviceName | The ovh kubernetes service name | `string` | `` | yes |
| ovh_endpoint | The ovh endpoint | `string` | `ovh-eu` | no |
| ovh_kubernetes_cluster_state | Specifies whether to create or delete the cluster | `Enum[present, absent]` | `present` | no |
| ovh_kubernetes_cluster_region | The cluster region | `string` | `SBG5` | no |
| ovh_kubernetes_cluster_version | The kubernetes version | `string` | `1.19` | no |
| ovh_kubernetes_cluster_desired_nodes | Number of node for the kubernetes cluster | `number` | `1` | no |
| ovh_kubernetes_cluster_flavor_name | The flavor of the kubernetes nodes  | `Enum[b2-7, b2-15, b2-30, b2-60, b2-120]` | `b2-7` | no |
| ovh_kubernetes_cluster_anti_affinity | Anti affinity for kubernetes nodes | `boolean` | `False` | no |
| ovh_kubernetes_cluster_monthly_billed | Bill monthly | `boolean` | `False` | no |
| ovh_kubernetes_cluster_path_kubeconfig | Path where the cluster kubeconfig will be created | `string` | `kubeconfig.yml` | no |
| ovh_kubernetes_cluster_tmp_dir | Path where the create cluster scripts will be created | `string` | `/tmp/ovh/kubernetes` | no |
