# -*- encoding: utf-8 -*-
'''
First, install the latest release of Python wrapper: $ pip install ovh
Then watch the api doc here: https://api.ovh.com/console/#/cloud/project/%7BserviceName%7D/kube#POST
'''
import os
import json
import ovh
import time
import requests


########################################################################
#
# OVH CONF
#
########################################################################
ovh_endpoint = os.environ['OVH_ENDPOINT']
ovh_application_key = os.environ['OVH_APPLICATION_KEY']
ovh_application_secret = os.environ['OVH_APPLICATION_SECRET']
ovh_consumer_key = os.environ['OVH_CONSUMER_KEY']
serviceName = os.environ['OVH_SERVICE_NAME']


client = ovh.Client(
    endpoint=ovh_endpoint,
    application_key=ovh_application_key,
    application_secret=ovh_application_secret,
    consumer_key=ovh_consumer_key
)


########################################################################
#
# CLUSTER CONF
#
########################################################################
cluster_name = os.environ['CLUSTER_NAME']


########################################################################
#
# METHODS
#
########################################################################
def get_kubernetes_clusters_ids():
    result = client.get(
        '/cloud/project/{serviceName}/kube'.format(serviceName=serviceName)
    )
    return result


def get_kubernetes_cluster_data(cluster_id):
    result = client.get(
        '/cloud/project/{serviceName}/kube/{cluster_id}'.format(
            serviceName=serviceName, cluster_id=cluster_id)
    )
    return result


def wait_for_cluster_to_be_deleted(cluster_id):
    retries = 0
    while True:
        clusters_ids = get_kubernetes_clusters_ids()
        if cluster_id not in clusters_ids:
            print('Cluster has been deleted')
            return

        retries += 1
        if retries > 100:
            raise Exception('Timeout: Cluster is too long to delete')
        time.sleep(10)


def delete_kubernetes_cluster(cluster_name):
    clusters_ids = get_kubernetes_clusters_ids()
    for cluster_id in clusters_ids:
        cluster_data = get_kubernetes_cluster_data(cluster_id)

        if cluster_data['name'] == cluster_name:
            print('Delete cluster: {}'.format(cluster_id))
            result = client.delete(
                '/cloud/project/{serviceName}/kube/{cluster_id}'.format(
                    serviceName=serviceName, cluster_id=cluster_id)
            )
            return

    print('No cluster match to delete')


delete_kubernetes_cluster(cluster_name)
