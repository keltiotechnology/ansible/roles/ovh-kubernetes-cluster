# -*- encoding: utf-8 -*-
'''
First, install the latest release of Python wrapper: $ pip install ovh
# /cloud/project/%7BserviceName%7D/kube#POST
Then watch the api doc here: https://api.ovh.com/console/
'''
import os
import hashlib
import json
import ovh
import time
import requests


def get_bool_in_env(key):
    str_boolean = os.getenv(key, default='False')

    return str_boolean.lower() in ("yes", "true", "t", "1")


########################################################################
#
# OVH CONF
#
########################################################################
ovh_endpoint = os.environ['OVH_ENDPOINT']
ovh_application_key = os.environ['OVH_APPLICATION_KEY']
ovh_application_secret = os.environ['OVH_APPLICATION_SECRET']
ovh_consumer_key = os.environ['OVH_CONSUMER_KEY']
serviceName = os.environ['OVH_SERVICE_NAME']

ENDPOINTS = {
    'ovh-eu': 'https://eu.api.ovh.com/1.0',
    'ovh-us': 'https://api.us.ovhcloud.com/1.0',
    'ovh-ca': 'https://ca.api.ovh.com/1.0',
    'kimsufi-eu': 'https://eu.api.kimsufi.com/1.0',
    'kimsufi-ca': 'https://ca.api.kimsufi.com/1.0',
    'soyoustart-eu': 'https://eu.api.soyoustart.com/1.0',
    'soyoustart-ca': 'https://ca.api.soyoustart.com/1.0',
}


########################################################################
#
# CLUSTER CONF
#
########################################################################
cluster_name = os.environ['CLUSTER_NAME']
region = os.environ['CLUSTER_REGION']
version = os.environ['CLUSTER_VERSION']
desired_nodes = int(os.environ['CLUSTER_DESIRED_NODES'])
flavor_name = os.environ['CLUSTER_FLAVOR_NAME']
anti_affinity = get_bool_in_env('CLUSTER_ANTI_AFFINITY')
monthly_billed = get_bool_in_env('CLUSTER_MONTHLY_BILLED')
path_to_save_kube_config = os.environ['CLUSTER_KUBECONFIG_PATH']

client = ovh.Client(
    endpoint=ovh_endpoint,
    application_key=ovh_application_key,
    application_secret=ovh_application_secret,
    consumer_key=ovh_consumer_key
)


########################################################################
#
# METHODS
#
########################################################################
def get_kubernetes_clusters_ids():
    result = client.get(
        '/cloud/project/{serviceName}/kube'.format(serviceName=serviceName)
    )
    return result


def get_kubernetes_cluster_data(cluster_id):
    result = client.get(
        '/cloud/project/{serviceName}/kube/{cluster_id}'.format(
            serviceName=serviceName, cluster_id=cluster_id)
    )
    return result


def wait_for_cluster_to_be_ready(cluster_data):
    cluster_id = cluster_data['id']
    status = cluster_data['status']
    retries = 0
    while status == 'INSTALLING':
        print('Waiting for the cluster to cluster to become ready...')
        cluster_data = get_kubernetes_cluster_data(cluster_id)
        status = cluster_data['status']
        time.sleep(5)
        retries += 1

        if retries > 100:
            raise Exception('To much time to create the cluster')
            break

    print('Cluster is ready')
    return cluster_data


def create_kubernetes_cluster(cluster_name, region, desired_nodes, flavor_name, anti_affinity, monthly_billed):
    clusters_ids = get_kubernetes_clusters_ids()
    print('Existing clusters')
    print(json.dumps(clusters_ids, indent=4))
    for cluster_id in clusters_ids:
        cluster_data = get_kubernetes_cluster_data(cluster_id)
        if cluster_data['name'] == cluster_name:
            print('Cluster already exist, no need to create')
            return cluster_data

    kube_data = {
        "name": cluster_name,
        "region": region,
        "version": version,
        "nodepool": {
            "desiredNodes": desired_nodes,
            "flavorName": flavor_name,
            "antiAffinity": anti_affinity,
            "monthlyBilled": monthly_billed
        }
    }
    cluster_data = client.post(
        '/cloud/project/{serviceName}/kube'.format(serviceName=serviceName), **kube_data
    )
    print(json.dumps(cluster_data, indent=4))
    return cluster_data


def get_kubernetes_cluster_kubeconfig(cluster_id):
    # CANNOT USE THE CLIENT FOR THIS METHOD
    # Issue: https://github.com/ovh/python-ovh/pull/85
    # Workflow réalisé à partir du code source: https://github.com/ovh/python-ovh/blob/master/ovh/client.py

    target = '{ovh_endpoint_url}/cloud/project/{serviceName}/kube/{cluster_id}/kubeconfig'.format(
        ovh_endpoint_url=ENDPOINTS[ovh_endpoint],
        serviceName=serviceName,
        cluster_id=cluster_id
    )
    body = ''
    now = str(int(time.time()))
    signature = hashlib.sha1()
    signature.update("+".join([
        ovh_application_secret,
        ovh_consumer_key,
        'POST',
        target,
        body,
        now
    ]).encode('utf-8'))
    headers = {
        'X-Ovh-Application': ovh_application_key,
        'X-Ovh-Consumer': ovh_consumer_key,
        'X-Ovh-Timestamp': now,
        'X-Ovh-Signature': "$1$" + signature.hexdigest()
    }

    result = requests.post(target, headers=headers)

    with open(path_to_save_kube_config, 'w') as f:
        f.write(result.json()['content'])
    print('Kubeconfig created')
    return result


cluster_data = create_kubernetes_cluster(
    cluster_name,
    region,
    desired_nodes,
    flavor_name,
    anti_affinity,
    monthly_billed
)

cluster_data = wait_for_cluster_to_be_ready(cluster_data)

get_kubernetes_cluster_kubeconfig(cluster_data['id'])
